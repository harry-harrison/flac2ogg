from unittest import main, TestCase
from tempfile import mkdtemp
from os import makedirs
from flac2ogg import isFlacFile, FlacFile

class TestFlacFile(TestCase):
    def test_oggFilename(self):
        ff = FlacFile("path/to/flac/file.flac","out/outdir")
        self.assertEqual("out/outdir/path/to/flac/file.ogg",ff.oggFilename())
        ff = FlacFile("path/to/flac/file.wibble","out/outdir")
        self.assertEqual("out/outdir/path/to/flac/file.ogg",ff.oggFilename())

    def test_oggFileExists(self):
        indir, outdir = self.createDirs()
        self.touch(indir+"/a.flac")
        self.touch(indir+"/b.flac")
        self.touch(outdir+"/a.ogg")
        a = FlacFile("a.flac",outdir)
        b = FlacFile("b.flac",outdir)
        self.assertEqual(True,a.oggFileExists())
        self.assertEqual(False,b.oggFileExists())

    def touch(self, filepath):
        with open(filepath, 'a') as _:
            pass

    def createDirs(self):
        dir = mkdtemp()
        indir = dir + "/in"
        outdir = dir + "/out"
        makedirs(indir)
        makedirs(outdir)
        return indir,outdir

    def test_isFlacFile(self):
        self.assertEqual(True,isFlacFile('this.flac'))
        self.assertEqual(False,isFlacFile('this.ogg'))
        self.assertEqual(False,isFlacFile('this.flac.ogg'))
        self.assertEqual(False,isFlacFile('flac.this'))

    def test_findFiles(self):
        self.fail()


    def test_findFlacPaths(self):
        self.fail()


    def test_filesNeedingConverting(self):
        self.fail()

    def test_convertFile(self):
        self.fail()

    def test_converter(self):
        self.fail()

    def test_multiThreadConvert(self):
        self.fail()


if __name__ == '__main__':
    main()