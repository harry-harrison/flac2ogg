#!/usr/bin/env python3
from os import path, walk
from multiprocessing.pool import Pool
from multiprocessing import cpu_count
from subprocess import check_output, CalledProcessError
import argparse


class FlacFile:
    def __init__(self, flacFilePath, outputDirectory):
        self.flacFilePath = flacFilePath
        self.outputDirectory = outputDirectory

    def oggFilename(self):
        return path.join(self.outputDirectory, path.splitext(self.flacFilePath)[0] + '.ogg')

    def oggFileExists(self):
        return path.exists(self.oggFilename())


def isFlacFile(filename):
    return '.flac' == str(path.splitext(filename)[1]).lower()


def findFiles(directory):
    for root, _, files in walk(directory):
        for f in files:
            yield path.relpath(path.join(root, f), directory)


def findFlacPaths(directory):
    return (x for x in findFiles(directory) if isFlacFile(x))


def filesNeedingConverting(inputDirectory, outputDirectory):
    files = (FlacFile(x, outputDirectory) for x in findFlacPaths(inputDirectory))
    return (x for x in files if x.oggFileExists())


def convertFile(flacFile, quality):
    infile = flacFile.flacFilePath
    outfile = flacFile.oggFilename()
    print("converting file: %s into %s" % (infile,outfile))
    try:
        result = check_output(['oggenc', '-q' + quality, infile, '-o', outfile])
        return (flacFile,True)
    except CalledProcessError as e:
        print("converting %s failed with output: %s " % (flacFile,e.output))
        return (flacFile,False)

def converter(quality):
    return lambda file : convertFile(file,quality)

def multiThreadConvert(threads, quality, inDir, outDir):
    print("Creating a Pool of %d threads to process flac files found in %s into ogg placed in %s" % (threads,inDir,outDir))
    with Pool(threads) as processPool:
        files = filesNeedingConverting(inDir, outDir)
        results = processPool.map(converter(quality), files)
        failures = (inputFile for (inputFile,success) in results if not success)
        print(failures)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Duplicate a directory structure of flac files into ogg files.')
    parser.add_argument('inputDirectory', metavar='indir', type=str, help='The directory to look for flac files')
    parser.add_argument('outputDirectory', metavar='outdir', type=str, help='The directory to put the ogg files')
    parser.add_argument('threads', metavar='threads', type=int, default=cpu_count()*2,help='The number of threads to use')
    parser.add_argument('encodingQuality', metavar='quality', type=int, default=6, help='The OGG quality to use')
    args = parser.parse_args()
    multiThreadConvert(args.threads, args.quality, args.inputDirectory, args.outputDirectory)
